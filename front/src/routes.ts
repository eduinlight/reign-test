import HomeRouter from "./routes/HomeRouter";
import { RouteType } from "./components/Routes";

export const paths = {
  HOME: "/"
};

export const routes: RouteType[] = [
  {
    path: paths.HOME,
    exact: true,
    component: HomeRouter,
    guards: []
  }
];
