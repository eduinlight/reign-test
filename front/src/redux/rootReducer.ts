import { combineReducers } from "redux";
import { apiReducers } from "./api/actions";

import formReducers from "./forms";

const rootReducer = combineReducers({
  api: combineReducers(apiReducers),
  forms: formReducers
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
