import { createStore, applyMiddleware } from "redux";
import rootReducer from "./rootReducer";
import saga, { END } from "redux-saga";
import rootSaga from "./rootSaga";
import logger from "redux-logger";

declare let module: any;

const initialState = {};

const sagaMiddleware = saga();

const middlewares = applyMiddleware(logger, sagaMiddleware);

const store = createStore(rootReducer, initialState, middlewares);

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
store.runSaga = sagaMiddleware.run(rootSaga);
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
store.slose = () => store.dispatch(END);

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept("./rootReducer", () => {
    // eslint-disable-next-line
    const nextRootReducer = require("./rootReducer").default;
    store.replaceReducer(nextRootReducer);
  });
}

export { store };
