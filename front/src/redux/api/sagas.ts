import { call, put, takeLatest } from "redux-saga/effects";
import { apiActions, apiCalls } from "./actions";

export const apiSagas = Object.keys(apiActions).map((k) => {
  const key = k as keyof typeof apiActions;
  const ajaxAction = apiActions[key];
  const apiCall = apiCalls[key];

  function* handler(action: any) {
    try {
      const response = yield call(apiCall, action.payload);
      yield put(ajaxAction.success(response.data));
    } catch (e) {
      console.log({ e });
      yield put(
        ajaxAction.failed({ status: e.response.status, data: e.response.data })
      );
    }
  }

  return takeLatest(ajaxAction.callType, handler);
});
