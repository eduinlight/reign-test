import NewsApi from "../../core/api/news.api";
import { FilterNewsParams, RemoveNewsParams } from "../../core/api/types";
import AjaxRequest from "../../core/interfaces/ajax_request";
import { createReducerAjax } from "../../utils/redux-actions-helper";
import AjaxAction from "../../utils/redux-actions-helper/AjaxAction";

export const apiActions = {
  filterNews: new AjaxAction<AjaxRequest, FilterNewsParams>("API_NEWS_FILTER"),
  removeNews: new AjaxAction<AjaxRequest, RemoveNewsParams>("API_NEWS_REMOVE")
};

export const apiReducers = {
  filterNews: createReducerAjax({ action: apiActions.filterNews }),
  removeNews: createReducerAjax({ action: apiActions.removeNews })
};

export const apiCalls = {
  filterNews: NewsApi.filter,
  removeNews: NewsApi.remove
};
