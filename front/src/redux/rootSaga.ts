import { all } from "redux-saga/effects";
import { apiSagas } from "./api/sagas";
import { newsSagas } from "./forms/news/sagas";

export default function* rootSaga() {
  yield all([...newsSagas, ...apiSagas]);
}
