import { combineReducers } from "redux";
import { newsReducer } from "./news/actions";

const formReducers = combineReducers({
  news: newsReducer
});

export default formReducers;
