import { put, takeLatest } from "redux-saga/effects";
import { newsActions } from "./actions";
import { apiActions } from "../../api/actions";

function* remove(action: any) {
  console.log("ACAA", action.payload);
  yield put(apiActions.removeNews.call({ id: action.payload }));
}

function* successFilter(action: any) {
  yield put(newsActions.addNews.call(action.payload));
}

function* successRemove() {
  yield put(newsActions.removeNews.success());
}

export const newsSagas = [
  takeLatest(newsActions.removeNews.callType, remove),
  takeLatest(apiActions.filterNews.successType, successFilter),
  takeLatest(apiActions.removeNews.successType, successRemove)
];
