import { INews } from "../../../core/interfaces/news";
import {
  Action,
  ComplexAction,
  createReducer
} from "../../../utils/redux-actions-helper";

type State = {
  news: INews[];
  skip: number;
  limit: number;
  hasMore: boolean;
};

const initialState = {
  news: [],
  skip: 0,
  limit: 10,
  hasMore: true
} as State;

class AddNews extends Action<State, INews[]> {
  callReducer = (state: State, payload: INews[]): void => {
    if (payload.length === 0) {
      state.hasMore = false;
    } else {
      state.news = [...state.news, ...payload];
    }
  };
}

class ShangePaginationNews extends Action<
  State,
  { skip: number; limit: number }
> {
  callReducer = (
    state: State,
    { skip, limit }: { skip: number; limit: number }
  ): void => {
    state.skip = skip;
    state.limit = limit;
  };
}

class RemoveNews extends ComplexAction<State, string, null, null> {
  private id = "";

  callReducer = (_state: State, payload: string): void => {
    this.id = payload;
  };

  successReducer = (state: State): void => {
    state.news = state.news.filter((n) => n.id !== this.id);
  };
}

export const newsActions = {
  addNews: new AddNews("NEWS_ADD"),
  changePagination: new ShangePaginationNews("NEWS_CHANGE_PAGINATION"),
  removeNews: new RemoveNews("NEWS_REMOVE")
};

export const newsReducer = createReducer(newsActions, initialState);
