import React, { FC, PropsWithChildren } from "react";
import "./style.scss";

export interface GuessTemplateProps {}

const GuessTemplate: FC<PropsWithChildren<GuessTemplateProps>> = (
  props: GuessTemplateProps
) => {
  const { children } = props;

  return (
    <>
      <header>
        <div>
          <h1>HN Feed</h1>
          <h5>{"We <3 hacker  news!"}</h5>
        </div>
      </header>
      <main>{children}</main>
    </>
  );
};

export default GuessTemplate;
