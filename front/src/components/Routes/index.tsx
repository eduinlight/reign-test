import React, { FunctionComponent, ReactNode } from "react";
import { Route, Switch } from "react-router";
// guards
import RouteWrapper from "./RouteWrapper";

export interface RouteType {
  path: string;
  exact: boolean;
  component: ReactNode;
  guards: Array<() => boolean>;
  guardsNotPassRedirect?: string;
}

export interface RoutersProps {
  routes: RouteType[];
}

const Routes: FunctionComponent<RoutersProps> = (props: RoutersProps) => {
  const { routes } = props;
  return (
    <Switch>
      {routes.map((route) => {
        return (
          <Route
            key={route.path}
            path={route.path}
            exact={route.exact}
            render={(props: any) => <RouteWrapper route={route} {...props} />}
          />
        );
      })}
    </Switch>
  );
};

export default Routes;
