import React from "react";
import { Redirect } from "react-router";
import { RouteType } from ".";

export interface RouteWrapperProps {
  route: RouteType;
}

const RouteWrapper = (props: any) => {
  const { route } = props;
  let ok = true;

  for (const guard of route.guards) {
    if (!guard()) {
      ok = false;
    }
  }

  if (!ok) {
    return <Redirect to={route.guardsNotPassRedirect} />;
  }

  return <route.component {...props} />;
};

export default RouteWrapper;
