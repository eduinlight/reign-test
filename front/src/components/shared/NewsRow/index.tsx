import React, { FC, useMemo, useState } from "react";
import * as dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { INews } from "../../../core/interfaces/news";
import "./styles.scss";
import removeImg from "../../../assets/images/remove.png";
import { useDispatch } from "react-redux";
import { newsActions } from "../../../redux/forms/news/actions";

dayjs.extend(relativeTime);

export interface NewsRowProps {
  news: INews;
}

const NewsRow: FC<NewsRowProps> = ({
  news: { id, url, title, author, createdAt }
}: NewsRowProps) => {
  const relativeDate = useMemo(() => dayjs(createdAt).fromNow(), [createdAt]);
  const [mouseOver, setMouseOver] = useState(false);
  const dispatch = useDispatch();

  const mouseEnter = () => {
    setMouseOver(true);
  };

  const mouseLeave = () => {
    setMouseOver(false);
  };

  const remove = (event: any) => {
    event.stopPropagation();
    dispatch(newsActions.removeNews.call(id));
  };

  const openLink = () => {
    window.open(url, "_blank");
  };

  return (
    <li
      title={title}
      onClick={openLink}
      onMouseEnter={mouseEnter}
      onMouseLeave={mouseLeave}
    >
      <span className="title">
        {title}
        <span className="author">- {author} -</span>
      </span>
      <div>
        <span className="date">{relativeDate}</span>
        <div className="btn-container">
          {mouseOver && (
            <button onClick={remove}>
              <img src={removeImg} title="remove this news" />
            </button>
          )}
        </div>
      </div>
    </li>
  );
};

export default NewsRow;
