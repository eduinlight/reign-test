import React, { ButtonHTMLAttributes, DetailedHTMLProps, FC } from "react";
import "./styles.scss";

export interface ButtonProps
  extends DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  title: string;
}

const Button: FC<ButtonProps> = (props: ButtonProps) => {
  const { title } = props;
  return <button {...props}>{title}</button>;
};

export default Button;
