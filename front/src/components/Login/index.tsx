import React, { FunctionComponent, useEffect } from "react";
import { Grid, Paper, Typography, Divider, Fade } from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LockIcon from "@material-ui/icons/Lock";
import { makeStyles } from "@material-ui/core/styles";
import FacebookButton from "../shared/FacebookButton";
import GoogleButton from "../shared/GoogleButton";
import Input from "../shared/Input";
import PasswordInput from "../shared/PasswordInput";
import SmallText from "../shared/SmallText";
import Button from "../shared/Button";
import Link from "../shared/Link";
import CheckBox from "../shared/CheckBox";
import TermsOfUse from "../shared/TermsOfUse";
import PrivacyPolicy from "../shared/PrivacyPolicy";
import { useTypedSelector } from "../../core/hooks/use_typed_selector";
import { newsActions, LoginModel } from "../../redux/forms/news/actions";
import { useDispatch } from "react-redux";
import useRoutes from "../../core/hooks/useRoutes";
import { translate } from "../../core/translate";
import useFirstLoad from "../../core/hooks/useFirstLoad";
import { apiActions } from "../../redux/api/actions";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      maxWidth: 400
    }
  }
}));

export interface LoginProps {}

const Login: FunctionComponent<LoginProps> = () => {
  const styles = useStyles();
  const dispatch = useDispatch();
  const { model, errors } = useTypedSelector((store) => store.forms.login);
  const { loading, success, error } = useTypedSelector(
    (store) => store.api.login
  );
  const { goToRedirect, goToRecoveryPassword } = useRoutes();
  const { firstLoad } = useFirstLoad([
    newsActions.reset.call(),
    apiActions.login.reset(),
    apiActions.loginSocial.reset()
  ]);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    dispatch(newsActions.submit.call());
  };

  const handleChange = (key: keyof LoginModel, value: never) => {
    dispatch(newsActions.onChange.call({ key, value }));
  };

  useEffect(() => {
    if (firstLoad && !loading && success) {
      goToRedirect();
    }
  }, [firstLoad, goToRedirect, loading, success]);

  return (
    <Fade in>
      <Paper className={styles.paper}>
        <form onSubmit={handleSubmit} noValidate autoComplete="off">
          <Grid container spacing={2} alignItems="center" direction="column">
            <Grid item>
              <Typography variant="h5">{translate("startSession")}</Typography>
              <Divider />
            </Grid>
            <FacebookButton />
            <Grid item></Grid>
            <Grid item>
              <GoogleButton />
            </Grid>
            <Grid item>
              <SmallText>{translate("loginStartingHolaPlace")}</SmallText>
            </Grid>
            <Grid item container>
              <Input
                error={Boolean(errors.email)}
                errorText={(Boolean(errors.email) && errors.email) || ""}
                value={model.email}
                onChange={(event: any) =>
                  handleChange("email", event.target.value as never)
                }
                icon={true}
                iconElement={<AccountCircleIcon />}
                required
                fullWidth
                label={translate("emailLabel")}
                inputContainerProps={{ className: "flex-1" }}
              />
            </Grid>
            <Grid item container>
              <PasswordInput
                error={Boolean(errors.password)}
                errorText={(Boolean(errors.password) && errors.password) || ""}
                value={model.password}
                onChange={(event: any) =>
                  handleChange("password", event.target.value as never)
                }
                icon={true}
                iconElement={<LockIcon />}
                required
                fullWidth
                label={translate("passwordLabel")}
                inputContainerProps={{ className: "flex-1" }}
              />
            </Grid>
            <Grid
              item
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item>
                <CheckBox label={translate("rememberMe")} />
              </Grid>
              <Grid item>
                <Link onClick={() => goToRecoveryPassword()}>
                  {translate("forgotPassword")}
                </Link>
              </Grid>
            </Grid>
            <Grid item container>
              <Button
                loading={loading && !error}
                disabled={loading}
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                text={
                  loading ? translate("starting") : translate("loginSubmitText")
                }
              />
            </Grid>
            <Grid item className="text-center">
              <SmallText display="inline">{translate("acceptOur")} </SmallText>{" "}
              <TermsOfUse />
              <SmallText display="inline">{translate("and")} </SmallText>{" "}
              <PrivacyPolicy />
            </Grid>
          </Grid>
        </form>
      </Paper>
    </Fade>
  );
};

export default Login;
