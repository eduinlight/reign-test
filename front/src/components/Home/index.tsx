import React, { FunctionComponent } from "react";
import { useDispatch } from "react-redux";
import useOnMount from "../../core/hooks/useOnMount";
import { useTypedSelector } from "../../core/hooks/use_typed_selector";
import { INews } from "../../core/interfaces/news";
import { apiActions } from "../../redux/api/actions";
import { newsActions } from "../../redux/forms/news/actions";
import GuessTemplate from "../layouts/GuessLayout";
import Button from "../shared/Button";
import NewsRow from "../shared/NewsRow";
import "./styles.scss";

export interface HomeProps {}

const Home: FunctionComponent<HomeProps> = () => {
  const dispatch = useDispatch();
  const { loading, error, success } = useTypedSelector(
    (store) => store.api.filterNews
  );
  const { hasMore, news, skip, limit } = useTypedSelector(
    (store) => store.forms.news
  );

  useOnMount(() => {
    dispatch(apiActions.filterNews.call({ skip, limit }));
  });

  const loadMore = () => {
    dispatch(newsActions.changePagination.call({ skip: skip + limit, limit }));
    dispatch(apiActions.filterNews.call({ skip: skip + limit, limit }));
  };

  return (
    <GuessTemplate>
      <ul className="news-container">
        {news.map((news: INews) => (
          <NewsRow key={news.id} news={news}></NewsRow>
        ))}
      </ul>
      {hasMore && (
        <div className="btn-container">
          <Button
            onClick={loadMore}
            title={loading ? "loading..." : "Load More"}
          />
        </div>
      )}
    </GuessTemplate>
  );
};

export default Home;
