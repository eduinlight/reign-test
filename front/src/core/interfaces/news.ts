export interface INews {
  id: string;
  title: string;
  url: string;
  author: string;
  objectID: string;
  createdAt: Date;
}
