export default interface AjaxRequest {
  loading: boolean;
  success: boolean;
  error: boolean;
  data: any;
  errors: any;
}

export const ajaxRequestInit: AjaxRequest = {
  loading: false,
  success: false,
  error: false,
  data: null,
  errors: {}
};
