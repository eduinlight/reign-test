import { useCallback } from "react";
import { paths } from "../../routes";
import { useHistory } from "react-router";
import { RegisterRouterLocationState } from "../../routes/RegisterRouter";
import { RecoveryPasswordRouterLocationState } from "../../routes/RecoveryPasswordRouter";

const useRoutes = () => {
  const navigator = useHistory();

  const goTo = useCallback(
    (path: string, state: any = {}, replace = false) => {
      if (replace) {
        navigator.replace(path, state);
      } else {
        navigator.push(path, state);
      }
    },
    [navigator]
  );

  const createGoTo = <T>(
    path: string,
    func: (() => void) | undefined = undefined
  ) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return (state: T = {}, replace = false) => {
      if (func) {
        func();
      }
      goTo(path, state, replace);
    };
  };

  return {
    goToHome: createGoTo(paths.HOME),
    goToUsersHome: createGoTo(paths.USERS_HOME),
    goToRedirect: createGoTo(paths.REDIRECT),
    goToLogin: createGoTo(paths.LOGIN),
    goToRegister: createGoTo<RegisterRouterLocationState>(paths.REGISTER),
    goToSpaces: createGoTo(paths.SPACES),
    goToReservations: createGoTo(paths.RESERVATIONS),
    goToProfile: createGoTo(paths.PROFILE),
    goToRecoveryPassword: createGoTo<RecoveryPasswordRouterLocationState>(
      paths.RECOVERY_PASSWORD
    )
  };
};

export default useRoutes;
