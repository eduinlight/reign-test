import { useCallback } from "react";
import useImmerState from "./useImmerState";
import { ClappyPreviewProps } from "../../components/shared/ClappyPreview";

function useClappyPreview() {
  const [state, setState] = useImmerState<Omit<ClappyPreviewProps, "history">>({
    open: false
  });

  const openPreview = useCallback(() => {
    setState((draft) => {
      draft.open = true;
    });
  }, [setState]);

  const onClose = useCallback(() => {
    setState((draft) => {
      draft.open = false;
    });
  }, [setState]);

  return {
    ...state,
    onClose,
    openPreview
  };
}

export default useClappyPreview;
