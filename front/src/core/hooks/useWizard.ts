import { useCallback, useMemo } from "react";
import useImmerState from "./useImmerState";

export enum WizardStepState {
  WAITING = "waiting",
  ACTIVE = "active",
  ERROR = "error",
  PASSED = "passed"
}

export interface WizardStep {
  state: WizardStepState;
  text: string;
}

const useWizard = (stepsTexts: string[]) => {
  const stepsTextTransformed = useMemo<WizardStep[]>(
    () =>
      stepsTexts.map(
        (text, index) =>
          ({
            state: (index === 0
              ? WizardStepState.ACTIVE
              : WizardStepState.WAITING) as WizardStepState,
            text
          } as WizardStep)
      ),
    [stepsTexts]
  );

  const [state, setState] = useImmerState({
    steps: stepsTextTransformed,
    activeStepIndex: 0
  });
  const { steps, activeStepIndex } = state;

  const hasNext = useMemo(() => activeStepIndex < steps.length - 1, [
    activeStepIndex,
    steps.length
  ]);

  const hasPrevious = useMemo(() => activeStepIndex > 0, [activeStepIndex]);

  const setStepState = useCallback(
    (step: number, newState: WizardStepState) => {
      setState((draft) => {
        draft.steps[step].state = newState;
      });
    },
    [setState]
  );

  const setActiveState = useCallback(
    (newState: WizardStepState) => {
      setState((draft) => {
        draft.steps[activeStepIndex].state = newState;
      });
    },
    [activeStepIndex, setState]
  );

  const reset = useCallback(() => {
    setState((draft) => {
      draft.steps = stepsTextTransformed;
      draft.activeStepIndex = 0;
    });
  }, [setState, stepsTextTransformed]);

  const setStep = useCallback(
    (index: number) => {
      setState((draft) => {
        draft.activeStepIndex = index;
        draft.steps[index].state = WizardStepState.ACTIVE;
      });
    },
    [setState]
  );

  const nextStep = useCallback(() => {
    if (hasNext) {
      setState((draft) => {
        const newIndex = activeStepIndex + 1;
        draft.activeStepIndex = newIndex;
        draft.steps[newIndex].state = WizardStepState.ACTIVE;
      });
    }
  }, [activeStepIndex, hasNext, setState]);

  const previousStep = useCallback(() => {
    if (hasPrevious) {
      setState((draft) => {
        const newIndex = activeStepIndex - 1;
        draft.activeStepIndex = newIndex;
        draft.steps[newIndex].state = WizardStepState.ACTIVE;
      });
    }
  }, [activeStepIndex, hasPrevious, setState]);

  return {
    steps,
    activeStepIndex,
    setActiveState,
    nextStep,
    hasNext,
    previousStep,
    hasPrevious,
    reset,
    setStep,
    setStepState
  };
};

export default useWizard;
