import { useCallback } from "react";
import Config from "../../../config";
import Storage from "../../../utils/storage";
import { ApiCall } from "../../api/types";
import Country from "../../interfaces/country";
import useQuery from "../useQuery";

const COUNTRIES_KEY = "hp-countries-data";

const useGetCountries = () => {
  const state = useQuery();

  const getCountries = useCallback(async (): Promise<Country[]> => {
    const data = Storage.load(COUNTRIES_KEY);
    if (data) {
      return Promise.resolve(data);
    }
    return new Promise((resolve, reject) => {
      state
        .sendRequest({
          url: `${Config.apiurl}/countries`,
          method: "get",
          sendAtStart: false
        } as ApiCall)
        .then((data: unknown) => {
          resolve(data as Country[]);
          Storage.save(COUNTRIES_KEY, data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }, [state]);

  return Object.assign(state, { getCountries });
};

export default useGetCountries;
