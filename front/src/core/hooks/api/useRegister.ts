import { useCallback } from "react";
import Config from "../../../config";
import { AuthStorageData } from "../../../redux/auth/types";
import { ApiCall } from "../../api/types";
import useQuery from "../useQuery";
import { ERole } from "../../interfaces/user.model";

export interface RegisterParams {
  email: string;
  name: string;
  lastName: string;
  birthdate: Date;
  phone: string;
  password: string;
  role: ERole;
}

const useRegister = () => {
  const state = useQuery<AuthStorageData>();

  const register = useCallback(
    (data: RegisterParams) => {
      return state.sendRequest({
        url: `${Config.apiurl}/auth/register`,
        method: "post",
        data,
        sendAtStart: false
      } as ApiCall);
    },
    [state]
  );

  return Object.assign(state, { register });
};

export default useRegister;
