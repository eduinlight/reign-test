import { useCallback } from "react";
import Config from "../../../config";
import { ApiCall } from "../../api/types";
import useQuery from "../useQuery";

export interface LoginParams {
  user: string;
  password: string;
}

const useLogin = () => {
  const state = useQuery();

  const login = useCallback(
    (data: LoginParams) => {
      return state.sendRequest({
        url: `${Config.apiurl}/auth/jwt/login`,
        method: "post",
        data,
        sendAtStart: false
      } as ApiCall);
    },
    [state]
  );

  return Object.assign(state, { login });
};

export default useLogin;
