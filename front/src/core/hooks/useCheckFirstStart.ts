import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useTypedSelector } from "./use_typed_selector";
import AppActions from "../../redux/app/actions";

const useCheckFirstStart = () => {
  const dispatch = useDispatch();
  const { firstStart } = useTypedSelector((store) => store.app);

  useEffect(() => {
    if (firstStart) {
      dispatch(AppActions.setFirstStart());
    }
  }, [dispatch, firstStart]);
};

export default useCheckFirstStart;
