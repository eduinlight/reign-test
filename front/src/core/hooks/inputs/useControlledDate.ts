import { useCallback } from "react";
import useImmerState from "../useImmerState";

export interface UseControlledDateParams {
  focus?: boolean;
  initialValue?: Date;
}

const useControlledDate = ({
  focus = false,
  initialValue = new Date()
}: UseControlledDateParams) => {
  const [{ value }, setState] = useImmerState({
    value: initialValue
  });

  const onChange = useCallback(
    (date: Date) => {
      setState((draft) => {
        draft.value = date;
      });
    },
    [setState]
  );

  return {
    props: {
      value,
      onChange
    }
  };
};

export default useControlledDate;
