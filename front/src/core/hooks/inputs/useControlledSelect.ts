import { useCallback } from "react";
import { SelectOption } from "../../../components/shared/Select";
import useImmerState from "../useImmerState";

export interface UseControlledDateParams {
  focus?: boolean;
  defaultValue?: SelectOption;
}

const useControlledSelect = ({
  focus = false,
  defaultValue = undefined
}: UseControlledDateParams) => {
  const [{ value }, setState] = useImmerState({
    value: defaultValue
  });

  const onChange = useCallback(
    (value: SelectOption) => {
      setState((draft) => {
        draft.value = value;
      });
    },
    [setState]
  );

  return {
    props: {
      value,
      onChange
    }
  };
};

export default useControlledSelect;
