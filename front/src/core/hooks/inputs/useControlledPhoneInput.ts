import { useCallback } from "react";
import { PhoneValue } from "../../../components/shared/PhoneInput";
import Validator from "../../../utils/validator";
import useImmerState from "../useImmerState";

export interface UseControlledPhoneInputParams {
  initialValue?: PhoneValue;
}

const useControlledPhoneInput = ({
  initialValue = { code: "+34", phone: "" }
}: UseControlledPhoneInputParams) => {
  const [{ value }, setState] = useImmerState({
    value: initialValue
  });

  const onChange = useCallback(
    (value: PhoneValue) => {
      setState((draft) => {
        draft.value = value;
      });
    },
    [setState]
  );

  const validateSilent = useCallback(() => {
    return Validator.validate(
      { input: value.phone },
      { input: ["required", "numeric"] }
    );
  }, [value.phone]);

  return {
    props: {
      value,
      onChange
    },
    validateSilent
  };
};

export default useControlledPhoneInput;
