import { useCallback, useMemo, useEffect, useRef } from "react";
import Validator from "../../../utils/validator";
import { IObjectRuleType, RuleType } from "../../../utils/validator/types";
import useImmerState from "../useImmerState";

export interface UseControlledInputParams {
  startfocused?: boolean;
  initialValue?: string;
  validations?: Array<RuleType | IObjectRuleType>;
}

const useControlledInput = ({
  startfocused = false,
  initialValue = "",
  validations = []
}: UseControlledInputParams) => {
  const initialState = useMemo(
    () => ({
      value: initialValue,
      error: false,
      errorText: "",
      focus: false
    }),
    [initialValue]
  );
  const [{ value, errorText, error, focus }, setState] = useImmerState(
    initialState
  );
  const inputRef = useRef<HTMLInputElement | null>(null);

  const setFocus = useCallback(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  const clear = useCallback(() => {
    setState((draft) => {
      Object.assign(draft, initialState);
    });
  }, [initialState, setState]);

  const setError = useCallback(
    (value: boolean, errorText?: string) => {
      setState((draft) => {
        draft.error = value;
        if (errorText) {
          draft.errorText = errorText;
        }
      });
    },
    [setState]
  );

  const validateSilent = useCallback(
    (_value: string = value) => {
      return Validator.validate({ input: _value }, { input: validations });
    },
    [validations, value]
  );

  const validate = useCallback(
    (_value: string = value) => {
      const validation = validateSilent(_value);
      console.log({ validation });
      if (!validation.valid) {
        setError(true, validation.errors[0].error);
      } else {
        setError(false);
      }
      return validation.valid;
    },
    [setError, validateSilent, value]
  );

  const onChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      setState((draft) => {
        draft.value = value;
      });
      // validate(value);
    },
    [setState]
  );

  useEffect(() => {
    if (startfocused) {
      setFocus();
    }
  }, [setFocus, startfocused]);

  const onFocus = useCallback(() => {
    setState((draft) => {
      draft.focus = true;
    });
  }, [setState]);

  const onBlur = useCallback(() => {
    setState((draft) => {
      draft.focus = false;
    });
    // validate(value);
  }, [setState]);

  return {
    props: {
      value,
      onChange,
      onFocus,
      onBlur,
      error,
      errorText,
      inputProps: {
        ref: inputRef
      }
    },
    focus,
    setFocus,
    setError,
    clear,
    validate,
    validateSilent
  };
};

export default useControlledInput;
