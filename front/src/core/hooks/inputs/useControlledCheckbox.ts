import { useCallback } from "react";
import useImmerState from "../useImmerState";

export interface UseControlledCheckboxParams {
  focus?: boolean;
  initialValue?: boolean;
}

const useControlledCheckbox = ({
  focus = false,
  initialValue = false
}: UseControlledCheckboxParams) => {
  const [{ checked }, setState] = useImmerState({
    checked: initialValue
  });

  const onChange = useCallback(
    (_event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
      setState((draft) => {
        draft.checked = checked;
      });
    },
    [setState]
  );

  return {
    props: {
      checked,
      onChange
    }
  };
};

export default useControlledCheckbox;
