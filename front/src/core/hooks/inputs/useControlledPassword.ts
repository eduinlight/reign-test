import { useCallback } from "react";
import useImmerState from "../useImmerState";
import useControlledInput from "./useControlledInput";
import { PasswordInputErrorState } from "../../../components/shared/InputConditions";

interface ErrorState {
  eightChars: PasswordInputErrorState;
  aNumber: PasswordInputErrorState;
  anUppercase: PasswordInputErrorState;
  showDescriptions: boolean;
}

export interface UseControlledPasswordParams {
  startfocused?: boolean;
  initialValue?: string;
}

const symbolRegex = /[@#$-/:-?{-~!"^_`\[\]0-9]/;
const upperRegex = /[A-Z]/;

const useControlledPassword = ({
  startfocused,
  initialValue
}: UseControlledPasswordParams) => {
  const input = useControlledInput({
    startfocused,
    initialValue,
    validations: [
      "required",
      { rule: "minLength", params: [8] },
      { rule: "contains", params: [symbolRegex] },
      { rule: "contains", params: [upperRegex] }
    ]
  });
  const [
    { eightChars, aNumber, anUppercase, showDescriptions },
    setState
  ] = useImmerState<ErrorState>({
    eightChars: "error",
    aNumber: "error",
    anUppercase: "error",
    showDescriptions: false
  });

  const validateSilent = useCallback(
    (_value: string = input.props.value) => {
      const moreThanEight = _value.length >= 8;
      const hasNumberOrSymbol = symbolRegex.test(_value);
      const hasUpper = upperRegex.test(_value);
      const valid = moreThanEight && hasNumberOrSymbol && hasUpper;

      setState((draft) => {
        draft.eightChars = moreThanEight ? "success" : "error";
        draft.aNumber = hasNumberOrSymbol ? "success" : "error";
        draft.anUppercase = hasUpper ? "success" : "error";
      });
      return valid;
    },
    [input, setState]
  );

  const validate = useCallback(
    (_value: string = input.props.value) => {
      const valid = validateSilent(_value);

      input.setError(!valid);

      return valid;
    },
    [input, validateSilent]
  );

  const onChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      input.props.onChange(event);
      validate(value);
    },
    [input.props, validate]
  );

  const onFocus = useCallback(() => {
    input.props.onFocus();
    setState((draft) => {
      draft.showDescriptions = true;
    });
  }, [input.props, setState]);

  const onBlur = useCallback(() => {
    input.props.onBlur();
  }, [input.props]);

  return {
    ...input,
    props: {
      ...input.props,
      onChange,
      onFocus,
      onBlur,
      eightChars,
      aNumber,
      anUppercase,
      showDescriptions
    },
    validate,
    validateSilent
  };
};

export default useControlledPassword;
