import { useCallback } from "react";
import useImmerState from "./useImmerState";
import { ClappyPreviewProps } from "../../components/shared/ClappyPreview";

function useClappyFinish() {
  const [state, setState] = useImmerState<ClappyPreviewProps>({
    open: false
  });

  const openFinish = useCallback(() => {
    setState((draft) => {
      draft.open = true;
    });
  }, [setState]);

  const onClose = useCallback(() => {
    setState((draft) => {
      draft.open = false;
    });
  }, [setState]);

  return {
    ...state,
    onClose,
    openFinish
  };
}

export default useClappyFinish;
