import { useState, useCallback } from "react";
import { useHistory } from "react-router-native";
import { paths } from "../../routes";
import useRoutes from "./useRoutes";

function useCamera(value: boolean) {
  const [isCameraOpen, setIsCameraOpen] = useState(value);
  const history = useHistory();
  const { goToEditHistory } = useRoutes();

  const openCamera = useCallback(() => {
    setIsCameraOpen(true);
  }, []);

  const closeCamera = useCallback(() => {
    goToEditHistory({}, true);
    setIsCameraOpen(false);
  }, [goToEditHistory]);

  const handleOnFinishRecord = (video: string) => {
    closeCamera();
    history.push(paths.ADD_VIDEO, { video });
  };

  return { isCameraOpen, openCamera, closeCamera, handleOnFinishRecord };
}

export default useCamera;
