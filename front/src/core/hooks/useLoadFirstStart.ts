import { useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import useOnMount from "./useOnMount";
import Storage from "../../utils/storage";
import { APP_STATE_FIRST_START } from "../../redux/app/reducer";
import AppActions from "../../redux/app/actions";

const useLoadFirstStart = (loadOnMount = true) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const loadFirstStart = useCallback(() => {
    setLoading(true);
    Storage.load(APP_STATE_FIRST_START)
      .then(() => {
        dispatch(AppActions.setFirstStart());
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [dispatch]);

  useOnMount(() => {
    if (loadOnMount) {
      loadFirstStart();
    }
  });

  return { loading, loadFirstStart };
};

export default useLoadFirstStart;
