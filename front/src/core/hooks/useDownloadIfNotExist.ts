import { useCallback } from "react";
import rnfs from "rn-fetch-blob";
import useDownloadQuery from "./useDownloadQuery";

const useDownloadIfNotExist = () => {
  const { downloadFile } = useDownloadQuery();

  const downloadIfNotExist = useCallback(
    async (url: string, downloadPath: string, sha1?: string) => {
      let responsePath = url;
      try {
        if (!(await rnfs.fs.exists(downloadPath))) {
          await downloadFile(url, downloadPath);
        } else if (sha1 !== (await rnfs.fs.hash(downloadPath, "sha1"))) {
          await downloadFile(url, downloadPath);
        }
        responsePath = downloadPath;
      } catch (e) {}

      return responsePath;
    },
    [downloadFile]
  );

  return { downloadIfNotExist };
};

export default useDownloadIfNotExist;
