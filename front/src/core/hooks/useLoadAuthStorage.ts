import { useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import { AUTH_STORAGE_KEY } from "../../redux/auth/reducer";
import { AuthStorageData } from "../../redux/auth/types";
import AuthActions from "../../redux/auth/actions";
import useOnMount from "./useOnMount";
import Storage from "../../utils/storage";

const useLoadAuthStorage = (loadOnMount: true) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const loadAuthfromStore = useCallback(() => {
    setLoading(true);
    Storage.load(AUTH_STORAGE_KEY)
      .then((data: AuthStorageData) => {
        dispatch(AuthActions.login(data));
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [dispatch]);

  useOnMount(() => {
    if (loadOnMount) {
      loadAuthfromStore();
    }
  });

  return { loading, loadAuthfromStore };
};

export default useLoadAuthStorage;
