import { useMemo } from "react";
import useUserData from "../useUserData";
import { ERole } from "../../interfaces/user.model";

const useIsHost = () => {
  const { isLogged, user } = useUserData();

  const isHost = useMemo(
    () => isLogged && user && user.rol && user.rol === ERole.HOST,
    [isLogged, user]
  );

  return { isHost };
};

export default useIsHost;
