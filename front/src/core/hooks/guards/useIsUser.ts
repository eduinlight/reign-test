import { useMemo } from "react";
import useUserData from "../useUserData";
import { ERole } from "../../interfaces/user.model";

const useIsUser = () => {
  const { isLogged, user } = useUserData();

  const isUser = useMemo(
    () => isLogged && user && user.rol && user.rol === ERole.USER,
    [isLogged, user]
  );

  return { isUser };
};

export default useIsUser;
