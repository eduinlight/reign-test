import { useCallback } from "react";
import useImmerState from "./useImmerState";
import { ClappyRemoveAlertProps } from "../../components/shared/ClappyRemoveAlert/index";
import useRoutes from "./useRoutes";

interface State {
  props: ClappyRemoveAlertProps;
  onSuccess?: () => void;
}

function useClappyFinishHistoryAlert() {
  const { goToClappyFinish } = useRoutes();
  const [state, setState] = useImmerState<State>({
    props: {
      title: "",
      message: "",
      loading: false,
      open: false
    }
  });

  const openAlert = useCallback(
    (title, message) => {
      setState((draft) => {
        draft.props.open = true;
        draft.props.title = title;
        draft.props.message = message;
      });
    },
    [setState]
  );

  const onCancel = useCallback(() => {
    setState((draft) => {
      draft.props.open = false;
    });
  }, [setState]);

  const onOk = useCallback(() => {
    goToClappyFinish({}, true);
  }, [goToClappyFinish]);

  return {
    props: {
      ...state.props,
      onCancel,
      onOk
    },
    openAlert
  };
}

export default useClappyFinishHistoryAlert;
