import { useCallback } from "react";
import useImmerState from "./useImmerState";
import { ApiCall } from "../api";
import { ClappyRemoveAlertProps } from "../../components/shared/ClappyRemoveAlert/index";
import useBuildAxiosRequest from "./useBuildAxiosRequest";
import Http from "../../utils/http";
import NotiService from "../services/noti.service";
import {translate} from "../translate";

interface State {
  props: ClappyRemoveAlertProps;
  apiCall: ApiCall | null;
  onSuccess?: () => void;
}

function useClappyRemoveAlert() {
  const [state, setState] = useImmerState<State>({
    props: {
      title: "",
      message: "",
      loading: false,
      open: false
    },
    apiCall: null
  });
  const { apiCall, onSuccess } = state;
  const { buildAxiosRequest } = useBuildAxiosRequest();

  const openAlert = useCallback(
    (title, message, _apiCall: ApiCall, _onSuccess = undefined) => {
      setState((draft) => {
        draft.props.open = true;
        draft.apiCall = _apiCall;
        draft.props.title = title;
        draft.props.message = message;
        draft.onSuccess = _onSuccess;
      });
    },
    [setState]
  );

  const onCancel = useCallback(() => {
    setState((draft) => {
      draft.props.open = false;
    });
  }, [setState]);

  const onOk = useCallback(() => {
    if (apiCall) {
      setState((draft) => {
        draft.props.loading = true;
      });
      Http(buildAxiosRequest(apiCall, {}))
        .then(() => {
          setState((draft) => {
            draft.props.loading = false;
            draft.props.open = false;
          });
          if (onSuccess) {
            onSuccess();
          }
          NotiService.success(translate("removeVideoSuccess"));
        })
        .catch((error) => {
          setState((draft) => {
            draft.props.loading = false;
            draft.props.open = false;
          });
          console.log(error);
          NotiService.danger(translate("removeVideoError"));
        });
    }
  }, [apiCall, buildAxiosRequest, onSuccess, setState, translate]);

  return {
    props: {
      ...state.props,
      onCancel,
      onOk
    },
    openAlert
  };
}

export default useClappyRemoveAlert;
