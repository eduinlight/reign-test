import { useState } from "react";
import { useDispatch } from "react-redux";
import useOnMount from "./useOnMount";

function useFirstLoad(actionsToDispatch: Array<any> = []) {
  const [firstLoad, setFirstLoad] = useState(false);
  const dispatch = useDispatch();

  useOnMount(() => {
    for (const action of actionsToDispatch) {
      dispatch(action);
    }
    setFirstLoad(true);
  });

  return { firstLoad };
}

export default useFirstLoad;
