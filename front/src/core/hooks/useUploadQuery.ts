import { useState, useCallback } from "react";
import { ApiCall } from "../api";
import { AxiosRequestConfig } from "axios";
import useQuery from "./useQuery";

const UPLOAD_HEADER = {
  headers: {
    "Content-Type": "application/x-www-form-urlencoded"
  }
};

const useUploadQuery = (
  apiCall?: ApiCall,
  options: AxiosRequestConfig = {}
) => {
  const [uploadPorcent, setUploadPorcent] = useState(0);

  const onUploadProgress = useCallback((progress: any) => {
    const { loaded, total } = progress;
    setUploadPorcent(parseInt(((loaded / total) * 100).toFixed(0), 10));
  }, []);

  const queryState = useQuery(apiCall, {
    ...options,
    ...UPLOAD_HEADER,
    onUploadProgress
  });

  return { ...queryState, uploadPorcent };
};

export default useUploadQuery;
