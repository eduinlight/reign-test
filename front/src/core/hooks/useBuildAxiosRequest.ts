import { useCallback } from "react";
import { useTypedSelector } from "./use_typed_selector";
import { AxiosRequestConfig } from "axios";
import { ApiCall, ApiCallMethod } from "../api/types";

const useBuildAxiosRequest = () => {
  const {
    auth: { token }
  } = useTypedSelector((store) => store.app);

  const buildAxiosRequest = useCallback(
    (apiCall: ApiCall, options: AxiosRequestConfig = {}) => {
      const { method, data, url } = apiCall;
      const request: AxiosRequestConfig = {
        method,
        data,
        url,
        headers: {},
        ...options
      };

      if (
        request.method === ApiCallMethod.DELETE ||
        request.method === ApiCallMethod.GET
      ) {
        delete request.data;
      }

      if (token !== "") {
        request.headers["x-access-token"] = token;
      }

      return request;
    },
    [token]
  );

  return { buildAxiosRequest };
};

export default useBuildAxiosRequest;
