import { useEffect } from "react";
import UrlUtils from "../../utils/url";
import Config from "../../config";
import { useDispatch } from "react-redux";
import DeeplinkActions from "../../redux/deeplink/actions";

const useInvitationDeepLink = (url: string) => {
  const dispatch = useDispatch();

  useEffect(() => {
    if (
      url !== "" &&
      url.startsWith(Config.firebase.deepLink.invitation.prefix)
    ) {
      const urlQuery = UrlUtils.getUrlQueryParams(url);
      if (urlQuery && urlQuery.link) {
        const referedQuery = UrlUtils.getUrlQueryParams(
          urlQuery.link as string
        );
        if (referedQuery && referedQuery.clappy) {
          dispatch(
            DeeplinkActions.setDeeplink({
              type: "invitation",
              value: referedQuery.clappy as string
            })
          );
        }
      }
    }
  }, [dispatch, url]);
};

export default useInvitationDeepLink;
