import { useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import useOnMount from "./useOnMount";
import Storage from "../../utils/storage";
import { APP_STATE_LANGUAGE } from "../../redux/app/reducer";
import AppActions from "../../redux/app/actions";
import * as RNLocalize from "react-native-localize";

const availableLanguages = ["en", "es", "fr"];

const useLoadLanguage = () => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const setDefaultLanguage = useCallback(() => {
    const languages = RNLocalize.getLocales();
    let ok = false;
    for (const language of languages) {
      if (availableLanguages.includes(language.languageCode) && !ok) {
        dispatch(AppActions.setLanguaje(language.languageCode as any));
        ok = true;
      }
    }
    if (!ok) {
      dispatch(AppActions.setLanguaje("en"));
    }
  }, [dispatch]);

  const loadLanguage = useCallback(() => {
    setLoading(true);
    Storage.load(APP_STATE_LANGUAGE)
      .then((value) => {
        dispatch(AppActions.setLanguaje(value));
        setLoading(false);
      })
      .catch(() => {
        setDefaultLanguage();
        setLoading(false);
      });
  }, [dispatch, setDefaultLanguage]);

  useOnMount(() => {
    loadLanguage();
  });

  return { loading, loadLanguage };
};

export default useLoadLanguage;
