import { useCallback } from "react";
import useImmerState from "./useImmerState";
import IMusic from "../interfases/music.model";
import { ClappyMusicSelectionProps } from "../../components/shared/ClappyMusicSelection";

function useClappyMusicSelection() {
  const [state, setState] = useImmerState<ClappyMusicSelectionProps>({
    open: false,
    music: undefined as any
  });

  const openMusicPlayer = useCallback(
    (music: IMusic) => () => {
      setState((draft) => {
        draft.music = music;
        draft.open = true;
      });
    },
    [setState]
  );

  const onClose = useCallback(() => {
    setState((draft) => {
      draft.open = false;
    });
  }, [setState]);

  return {
    open: state.open,
    music: state.music,
    onClose,
    openMusicPlayer
  };
}

export default useClappyMusicSelection;
