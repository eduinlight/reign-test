import { useEffect, useCallback } from "react";
import Http from "../../utils/http";
import { AxiosRequestConfig, AxiosPromise } from "axios";
import useBuildAxiosRequest from "./useBuildAxiosRequest";
import useQueryState from "./useQueryState";
import { ApiCall } from "../interfases/api";

const useQuery = <T>(apiCall?: ApiCall, options: AxiosRequestConfig = {}) => {
  const { state, setLoading, setError, setSuccess } = useQueryState<T>();
  const { loading, error, success, data } = state;
  const { buildAxiosRequest } = useBuildAxiosRequest();

  const handlePrimese = useCallback(
    (promise: AxiosPromise<any>): Promise<T> => {
      setLoading();
      return new Promise<T>((resolve, reject) => {
        promise
          .then((response) => {
            console.log("RESPONSE: A", response.data);
            setSuccess(response.data);
            resolve(response.data);
          })
          .catch((_error) => {
            console.log(JSON.stringify(_error, null, 2));
            setError(_error);
            reject(_error);
          });
      });
    },
    [setError, setLoading, setSuccess]
  );

  const sendRequest = useCallback(
    (requestApiCall?: ApiCall): Promise<T> => {
      let request = null;
      const optionsWithCancel = {
        ...options
      };

      if (requestApiCall) {
        request = buildAxiosRequest(requestApiCall, optionsWithCancel);
      } else if (apiCall) {
        request = buildAxiosRequest(apiCall, optionsWithCancel);
      }

      console.log("REQUEST: ", { request }, null, 2);

      if (request) {
        return handlePrimese(Http(request));
      }

      throw new Error(
        "you must specify an ApiCall to sendRequest Method or useQuery hook param"
      );
    },
    [apiCall, buildAxiosRequest, handlePrimese, options]
  );

  useEffect(() => {
    if (apiCall && apiCall.sendAtStart) {
      sendRequest();
    }
  }, [apiCall, sendRequest]);

  return {
    loading,
    error,
    success,
    data,
    sendRequest
  };
};

export default useQuery;
