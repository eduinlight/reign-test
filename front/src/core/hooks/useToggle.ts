import { useState, useCallback } from "react";

function useToggle(value: boolean): [boolean, () => void] {
  const [state, setState] = useState(value);

  const toggleValue = useCallback(() => {
    setState(!state);
  }, [state]);

  return [state, toggleValue];
}

export default useToggle;
