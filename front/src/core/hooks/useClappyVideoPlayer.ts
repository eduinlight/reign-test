import { useCallback } from "react";
import useImmerState from "./useImmerState";
import { ClappyVideoPlayerProps } from "../../components/shared/ClappyVideoPlayer/index";

function useClappyVideoPlayer() {
  const [state, setState] = useImmerState<ClappyVideoPlayerProps>({
    open: false,
    path: ""
  });

  const openVideoPlayer = useCallback(
    (uri: string) => () => {
      setState((draft) => {
        draft.open = true;
        draft.path = uri;
      });
    },
    [setState]
  );

  const onClose = useCallback(() => {
    setState((draft) => {
      draft.open = false;
    });
  }, [setState]);

  return {
    ...state,
    onClose,
    openVideoPlayer
  };
}

export default useClappyVideoPlayer;
