import { useMemo } from "react";
import { useTypedSelector } from "./use_typed_selector";

const useUserData = () => {
  const { user, token } = useTypedSelector((store) => store.app.auth);
  const isLogged = useMemo(() => token && user, [token, user]);

  return { user, token, isLogged };
};

export default useUserData;
