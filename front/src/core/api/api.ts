import queryString from "query-string";

export default abstract class Api {
  path: string;

  constructor(path: string) {
    this.path = path;
  }

  buildPath = (path: string, query?: Record<string, any>) => {
    const queryStr = query ? `?${queryString.stringify(query)}` : "";
    return `${this.path}${path}${queryStr}`;
  };
}
