import Http from "../../utils/http";
import Api from "./api";
import { FilterNewsParams, RemoveNewsParams } from "./types";

class NewsApiClass extends Api {
  constructor() {
    super("/news");
  }

  filter = ({ skip = 0, limit = 20 }: FilterNewsParams) => {
    return Http.get(this.buildPath("/", { skip, limit }));
  };

  remove = ({ id }: RemoveNewsParams) => {
    console.log({id})
    return Http.delete(this.buildPath(`/${id}`));
  };
}

const NewsApi = new NewsApiClass();

export default NewsApi;
