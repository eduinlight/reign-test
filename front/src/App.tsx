import React, { FunctionComponent } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import "./styles.scss";
import "typeface-roboto";
import { routes } from "./routes";
import Routes from "./components/Routes";

const App: FunctionComponent = () => {
  return (
    <Provider store={store}>
      <Router>
        <Routes routes={routes} />
      </Router>
    </Provider>
  );
};

export default App;
