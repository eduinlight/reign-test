import React, { FunctionComponent } from "react";
import Home from "../../components/Home";

interface HomeRouterProps {}

const HomeRouter: FunctionComponent<HomeRouterProps> = () => {
  return <Home />;
};

export default HomeRouter;
