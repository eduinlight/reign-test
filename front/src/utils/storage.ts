class StorageClass {
  public load(key: string): any | null {
    const data = localStorage.getItem(key);
    if (data) {
      return JSON.parse(data);
    }
    return null;
  }

  public async save(key: string, data: any) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  public async delete(key: string) {
    localStorage.removeItem(key);
  }
}

const Storage = new StorageClass();

export default Storage;
