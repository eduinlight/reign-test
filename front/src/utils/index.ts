import Http from "./http";
import Storage from "./storage";

export default { Http, Storage };
