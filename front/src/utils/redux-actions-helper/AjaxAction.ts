import AjaxRequest, {
  ajaxRequestInit
} from "../../core/interfaces/ajax_request";
import ComplexAction from "./ComplexAction";

class AjaxAction<T extends AjaxRequest, CallType> extends ComplexAction<
  T,
  CallType,
  any,
  any
> {
  callReducer = (state: AjaxRequest) => {
    Object.assign(state, { ...ajaxRequestInit, loading: true });
  };

  successReducer = (state: AjaxRequest, payload: any): void => {
    state.loading = false;
    state.success = true;
    state.data = payload;
    state.errors = {};
  };

  failedReducer = (state: AjaxRequest, payload: any) => {
    state.loading = false;
    state.error = true;
    state.errors = payload;
  };

  resetReducer = (state: AjaxRequest) => {
    Object.assign(state, ajaxRequestInit);
  };
}

export default AjaxAction;
