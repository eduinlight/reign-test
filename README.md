# Reign test 
- name: Eduin Garcia Cordero
- linkedin: [https://www.linkedin.com/in/eduinlight/](https://www.linkedin.com/in/eduinlight/)
- github: [https://github.com/eduinlight](https://github.com/eduinlight)
- email: [eduinlight@gmail.com](mailto:eduinlight@gmail.com)

## Some notes

I upload the .env file because there is no sensible information.

## Documentation
- [frontend](https://gitlab.com/eduinlight/reign-test/-/blob/master/front/README.md)
- [backend](https://gitlab.com/eduinlight/reign-test/-/blob/master/api/README.md)
