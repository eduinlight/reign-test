import { HttpModule } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import { FetchNewsService } from './fetch_news.service';

describe('FecthNewsService', () => {
  let fetchNewsService: FetchNewsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        HttpModule,
        WinstonModule.forRoot({
          level: 'info',
          format: winston.format.json(),
          defaultMeta: { service: 'user-service' },
          transports: [
            new winston.transports.Console({
              format: winston.format.simple(),
            }),
          ],
        }),
      ],
      providers: [FetchNewsService],
    }).compile();

    fetchNewsService = moduleRef.get<FetchNewsService>(FetchNewsService);
  });

  describe('fetchNewsPerPage', () => {
    it('should fetch data from nodejs news api', async () => {
      const page = 0;
      const newsPerPage = 20;

      const news = await fetchNewsService.fetchNewsPerPage(page, newsPerPage);

      expect(news).toHaveLength(newsPerPage);
    });
  });
});
