import { HttpService, Inject, Injectable } from '@nestjs/common';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { buildNewsUrl } from '../core';
import {
  INewsAlgolia,
  INewsAlgoliaResponse,
} from '../interfaces/news_algolia.interface';

@Injectable()
export class FetchNewsService {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
    private readonly http: HttpService,
  ) {}

  fetchNewsPerPage(page = 0, hitsPerPage = 100): Promise<INewsAlgolia[]> {
    this.logger.info('FETCHING NEWS REFRESH');
    return new Promise((resolve, reject) => {
      this.http.get(buildNewsUrl(page, hitsPerPage)).subscribe(
        (response) => {
          this.logger.info('FETCHING NEWS - SUCCESS');
          resolve((response.data as INewsAlgoliaResponse).hits || []);
        },
        (error) => {
          this.logger.error('FETCHING NEWS - ERROR');
          reject(error);
        },
      );
    });
  }

  fetchAllNews(): Promise<INewsAlgolia[]> {
    return this.fetchNewsPerPage(0, Number.MAX_SAFE_INTEGER);
  }
}
