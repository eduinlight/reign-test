import { Inject, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { NewsService } from './news.service';

@Injectable()
export class BootstrapNewsService implements OnApplicationBootstrap {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
    private readonly newsService: NewsService,
  ) {}

  async onApplicationBootstrap() {
    try {
      this.logger.info('REFRESH NEWS ON APPLICATION START');
      await this.newsService.refresh();
      this.logger.info('REFRESH NEWS ON APPLICATION START - SUCCESS');
    } catch (e) {
      this.logger.warn('REFRESH NEWS ON APPLICATION START - ERROR: ', e);
    }
  }
}
