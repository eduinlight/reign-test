import { Injectable } from '@nestjs/common';
import { QueryFactoryService } from '../../data_manager/services/query_factory.service';
import { NewsRepository } from '../../data_manager/repositories/news.repository';
import { INews } from '../../data_manager/schemas/news.schema';
import { FetchNewsService } from './fetch_news.service';

@Injectable()
export class NewsService {
  constructor(
    private readonly queryFactoryServide: QueryFactoryService,
    private readonly newsRepository: NewsRepository,
    private readonly fetchNewsService: FetchNewsService,
  ) {}

  private async mapNewsOnDb(): Promise<Map<string, boolean>> {
    const dbNews = await this.newsRepository.filter({ select: 'objectID' });
    const map = new Map<string, boolean>();

    for (const news of dbNews) {
      map.set(news.objectID, true);
    }

    return map;
  }

  async refresh() {
    try {
      const map = await this.mapNewsOnDb();
      const hits = await this.fetchNewsService.fetchAllNews();

      for (const hit of hits) {
        if (!map.get(hit.objectID) && (hit.story_title || hit.title)) {
          this.newsRepository.create({
            objectID: hit.objectID,
            title: hit.story_title || hit.title,
            author: hit.author,
            createdAt: hit.created_at,
            url: hit.story_url || hit.url,
          });
        }
      }
    } catch (e) {}
  }

  async filter(skip = 0, limit = 20): Promise<INews[]> {
    return await this.newsRepository.filter({
      skip,
      limit,
      sort: '-createdAt',
    });
  }

  async remove(id: string): Promise<boolean> {
    return this.newsRepository.delete(this.queryFactoryServide.findById(id));
  }
}
