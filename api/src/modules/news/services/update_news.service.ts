import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { NewsService } from './news.service';

@Injectable()
export class UpdateNewsService {
  constructor(private readonly newsService: NewsService) {}

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    this.newsService.refresh();
  }
}
