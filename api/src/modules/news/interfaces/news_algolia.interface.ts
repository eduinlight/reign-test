export interface INewsAlgolia {
  objectID: string;
  created_at: Date;
  title: null | string;
  story_title: null | string;
  story_url: null | string;
  url: null | string;
  author: string;
}

export interface INewsAlgoliaResponse {
  hits: [INewsAlgolia];
}
