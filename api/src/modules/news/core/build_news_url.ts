const buildNewsUrl = (page = 0, hitsPerPage = 100) => {
  return `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&page=${page}&hitsPerPage=${hitsPerPage}`;
};

export default buildNewsUrl;
