import buildNewsUrl from './build_news_url';

describe('news api url builder', () => {
  it('should generate correct url', () => {
    const generated = `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&page=10&hitsPerPage=20`;

    expect(buildNewsUrl(10, 20)).toBe(generated);
  });
});
