import { HttpModule, Module } from '@nestjs/common';
import { DataManagerModule } from '../data_manager/data_manager.module';
import { BootstrapNewsService } from './services/bootstrap.service';
import { FetchNewsService } from './services/fetch_news.service';
import { NewsService } from './services/news.service';
import { UpdateNewsService } from './services/update_news.service';

@Module({
  imports: [HttpModule, DataManagerModule],
  providers: [
    BootstrapNewsService,
    NewsService,
    FetchNewsService,
    UpdateNewsService,
  ],
  exports: [NewsService],
})
export class NewsModule {}
