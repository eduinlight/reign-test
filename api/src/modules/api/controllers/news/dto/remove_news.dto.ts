import { IsString } from 'class-validator';

export class RemoveNewsDto {
  @IsString()
  id: string;
}
