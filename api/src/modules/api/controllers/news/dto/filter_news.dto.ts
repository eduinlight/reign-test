import { IsNumberString, IsOptional } from 'class-validator';

export class FilterNewsDto {
  @IsOptional()
  @IsNumberString()
  skip: string;

  @IsOptional()
  @IsNumberString()
  limit: string;
}
