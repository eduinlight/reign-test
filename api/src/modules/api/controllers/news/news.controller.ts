import { Controller, Delete, Get, Param, Query } from '@nestjs/common';
import { NewsService } from '../../../news/services/news.service';
import { FilterNewsDto } from './dto/filter_news.dto';
import { RemoveNewsDto } from './dto/remove_news.dto';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get()
  async filter(@Query() { skip = '0', limit = '20' }: FilterNewsDto) {
    return this.newsService.filter(parseInt(skip), parseInt(limit));
  }

  @Delete(':id')
  async remove(@Param() { id }: RemoveNewsDto) {
    return this.newsService.remove(id);
  }
}
