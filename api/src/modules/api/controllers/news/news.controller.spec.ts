import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { NewsService } from '../../../news/services/news.service';
import * as request from 'supertest';
import { NewsController } from './news.controller';
import { NewsServiceMock } from './mock/news_service.mock';

describe('News e2e', () => {
  let app: INestApplication;
  const newsService = new NewsServiceMock();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [NewsService],
      controllers: [NewsController],
    })
      .overrideProvider(NewsService)
      .useValue(newsService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET news`, () => {
    return request(app.getHttpServer())
      .get('/news')
      .expect(200)
      .expect(newsService.filter());
  });

  it(`/GET news?skip=1`, () => {
    return request(app.getHttpServer())
      .get('/news?skip=1')
      .expect(200)
      .expect(newsService.filter(1));
  });

  it(`/GET news skip=1 limit=1`, () => {
    return request(app.getHttpServer())
      .get('/news?skip=1&limit=1')
      .expect(200)
      .expect(newsService.filter(1, 1));
  });

  it(`/DELETE news id=news2`, () => {
    return request(app.getHttpServer()).delete('/news/news2').expect(200);
  });

  it(`/GET news for check removed element`, () => {
    return request(app.getHttpServer())
      .get('/news')
      .expect(200)
      .expect(newsService.filter().filter((n) => n !== 'news2'));
  });

  afterAll(async () => {
    await app.close();
  });
});
