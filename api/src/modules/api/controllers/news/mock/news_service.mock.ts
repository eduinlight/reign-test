import { Injectable } from '@nestjs/common';

@Injectable()
export class NewsServiceMock {
  private news: string[];

  constructor() {
    this.news = ['news1', 'news2', 'news3', 'news4'];
  }

  filter(skip = 0, limit = 20): string[] {
    if (skip > this.news.length - 1) {
      return [];
    }

    return [...this.news].splice(skip, limit);
  }

  remove(id: string): boolean {
    const newsFiltered = this.news.filter((n) => n !== id);
    const ok = newsFiltered.length < this.news.length;
    this.news = newsFiltered;

    return ok;
  }
}
