import { Module } from '@nestjs/common';
import { NewsModule } from '../news/news.module';
import { NewsController } from './controllers/news/news.controller';

@Module({
  imports: [NewsModule],
  controllers: [NewsController],
})
export class ApiModule {}
