import { Document, FilterQuery, Model } from 'mongoose';
import IModel from './imodel';

export default abstract class Repository<
  TDataDocument extends Document & TData,
  TData extends IModel
> {
  protected model: Model<TDataDocument>;

  constructor(model: Model<TDataDocument>) {
    this.model = model;
  }

  public exists = async (
    condition: FilterQuery<TDataDocument>,
  ): Promise<boolean> => {
    return await this.model.exists(condition);
  };

  public count = async (
    condition: FilterQuery<TDataDocument>,
  ): Promise<number> => {
    return await this.model.countDocuments(condition);
  };

  public getAll = async (): Promise<TData[]> => {
    return (await this.model.find({})) as TData[];
  };

  public filterOne = async (
    condition: FilterQuery<TDataDocument>,
  ): Promise<TData | null> => {
    const result = await this.model.findOne(condition);
    if (result) {
      return result as TData;
    }
    return null;
  };

  public filter = async ({
    condition = {},
    skip = 0,
    limit = Number.MAX_SAFE_INTEGER,
    sort = '',
    select = '',
  }: {
    condition?: FilterQuery<TDataDocument>;
    skip?: number;
    limit?: number;
    sort?: string;
    select?: string | any;
  }): Promise<TData[]> => {
    return (await this.model
      .find(condition)
      .skip(skip)
      .limit(limit)
      .select(select)
      .sort(sort)) as TData[];
  };

  public get = async (id: string): Promise<TData | null> => {
    const result = await this.model.findById(id);
    if (result) {
      return result as TData;
    }
    return null;
  };

  public create = async (data: TData): Promise<TData> => {
    return (await this.model.create(data)) as TData;
  };

  public update = async (
    condition: FilterQuery<TDataDocument>,
    data: Partial<TData>,
  ): Promise<TData> => {
    return (await this.model.findOneAndUpdate(
      condition,
      { $set: data } as any,
      {
        new: true,
      },
    )) as TData;
  };

  public updateAll = async (
    condition: FilterQuery<TDataDocument>,
    data: Partial<TData>,
  ): Promise<void> => {
    await this.model.updateMany(condition, {
      $set: data,
    } as any);
  };

  public createOrUpdate = async (
    condition: FilterQuery<TDataDocument>,
    data: Partial<TData>,
  ): Promise<TData> => {
    return (await this.model.findOneAndUpdate(
      condition,
      {
        $set: data,
      } as any,
      { new: true, upsert: true },
    )) as TData;
  };

  public delete = async (
    condition: FilterQuery<TDataDocument>,
  ): Promise<boolean> => {
    const deleteResult = await this.model.deleteOne(condition);
    return deleteResult.deletedCount > 0;
  };

  public deleteAll = async (
    condition: FilterQuery<TDataDocument>,
  ): Promise<boolean> => {
    const deleteResult = await this.model.deleteMany(condition);
    return deleteResult.deletedCount > 0;
  };

  public deleteAndReturn = async (
    condition: FilterQuery<TDataDocument>,
  ): Promise<TData> => {
    const data = await this.model.findOneAndDelete(condition);
    return data as TData;
  };

  public aggregate = async (pipeline: any): Promise<any> => {
    const result = await this.model.aggregate(pipeline);
    if (result) {
      return result as TData[];
    }
    return null;
  };
}
