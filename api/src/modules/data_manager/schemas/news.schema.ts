import { Schema, Document } from 'mongoose';
import IModel from '../core/imodel';

export interface INews extends IModel {
  id?: Document['_id'];
  title: string;
  url: string;
  author: string;
  objectID: string;
}

export type NewsDocument = INews & Document;

export const newsSchema: Schema = new Schema(
  {
    title: String,
    author: String,
    url: String,
    objectID: String,
  },
  {
    timestamps: true,
    toJSON: {
      transform: (_: any, ret: any): any => {
        const id = ret._id;
        delete ret.__v;
        delete ret.password;
        delete ret._id;
        delete ret.validationCode;

        return {
          id,
          ...ret,
        };
      },
    },
  },
);
