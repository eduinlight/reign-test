import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsRepository } from './repositories/news.repository';
import { newsSchema } from './schemas/news.schema';
import { QueryFactoryService } from './services/query_factory.service';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get('database.mongodb.uri'),
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      }),
    }),
    MongooseModule.forFeature([{ name: 'News', schema: newsSchema }]),
  ],
  providers: [NewsRepository, QueryFactoryService],
  exports: [NewsRepository, QueryFactoryService],
})
export class DataManagerModule {}
