import { Schema } from 'mongoose';

const createMongooseMiddleware = (fields: string[]) =>
  function (next: any): void {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    let previous = this;
    for (const field of fields) {
      previous = previous.populate(field);
    }
    next();
  };

function autoPopulateSchema(
  schema: Schema,
  fields: string[],
  hooks = ['findOne', 'find', 'findOneAndUpdate', 'save'],
): void {
  const autoPopulate = createMongooseMiddleware(fields);
  for (const hook of hooks) {
    schema.pre(hook, autoPopulate);
  }
}

export default autoPopulateSchema;
