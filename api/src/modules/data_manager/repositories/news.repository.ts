import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import Repository from '../core/repository';
import { INews, NewsDocument } from '../schemas/news.schema';
import { QueryFactoryService } from '../services/query_factory.service';

@Injectable()
export class NewsRepository extends Repository<NewsDocument, INews> {
  constructor(
    @InjectModel('News') private readonly newsModel: Model<NewsDocument>,
    private readonly queryFactoryService: QueryFactoryService,
  ) {
    super(newsModel);
  }

  async getLatestNews(): Promise<INews | null> {
    const news = await this.filter({ sort: '-createdAt', limit: 1 });
    if (news.length > 0) {
      return null;
    }
    return news[0];
  }

  getNewsAfter(date: Date): Promise<INews[]> {
    return this.filter({
      condition: this.queryFactoryService.gt('createdAt', date),
      sort: '-createdAt',
    });
  }
}
