import { Injectable } from '@nestjs/common';

@Injectable()
export class QueryFactoryService {
  id = '_id';

  public or(or: Array<any>): any {
    return {
      $or: or,
    };
  }

  public lowerThan(a: any, b: any): any {
    return {
      [a]: { $lt: b },
    };
  }

  public and(and: Array<any>): any {
    return {
      $and: and,
    };
  }

  public findById(id: string): any {
    return {
      [this.id]: id,
    };
  }

  public gt(a: string, b: any): any {
    return {
      [a]: { $gt: b },
    };
  }

  public lt(a: string, b: any): any {
    return {
      [a]: { $lt: b },
    };
  }

  public lte(a: string, b: any): any {
    return {
      [a]: { $lte: b },
    };
  }

  public gte(a: string, b: any): any {
    return {
      [a]: { $gte: b },
    };
  }

  public notEqual(a: any, b: any): any {
    return {
      [a]: { $ne: b },
    };
  }

  public in(a: any, b: Array<any>): any {
    const query = {
      [a]: { $in: b },
    };
    return query;
  }

  public notIn(a: any, b: Array<any>): any {
    return {
      [a]: { $nin: b },
    };
  }

  public contains(
    a: Record<string, { value: string; options: 'i' | 'g' }>,
  ): any {
    const condition = {};

    for (const key in a) {
      condition[key] = {
        $regex: a[key].value,
        $options: 'i',
      };
    }

    return condition;
  }

  public existsAndNotNull(a: string, b = true): any {
    return {
      [a]: { $exists: b, $ne: null },
    };
  }

  public existsAndNull(a: string, b = true): any {
    return {
      [a]: { $exists: b, $eq: null },
    };
  }
}
