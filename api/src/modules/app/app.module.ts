import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { config, envConfigSchema } from './config';
import { ApiModule } from '../api/api.module';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import * as rotateFile from 'winston-daily-rotate-file';
import { AppService } from './app.service';
import { DataManagerModule } from '../data_manager/data_manager.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      envFilePath: ['.env'],
      load: [config],
      validationSchema: envConfigSchema,
      isGlobal: true,
    }),
    DataManagerModule,
    ApiModule,
    WinstonModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const isProd = configService.get('env') === 'production';
        return isProd
          ? {
              level: 'info',
              format: winston.format.json(),
              defaultMeta: { service: 'user-service' },
              transports: [
                new winston.transports.File({
                  filename: 'logs/error.log',
                  level: 'error',
                }),
                new winston.transports.Console({
                  format: winston.format.simple(),
                }),
                new rotateFile({
                  filename: 'logs/application-%DATE%.log',
                  datePattern: 'YYYY-MM-DD',
                  zippedArchive: true,
                  maxSize: '20m',
                  maxFiles: '30d',
                }),
              ],
            }
          : {
              level: 'info',
              format: winston.format.json(),
              defaultMeta: { service: 'user-service' },
              transports: [
                new winston.transports.Console({
                  format: winston.format.simple(),
                }),
              ],
            };
      },
    }),
  ],
  providers: [AppService],
})
export class AppModule {}
