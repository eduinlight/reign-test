const config = () => {
  const { NODE_ENV, PORT, BASE_URL, MONGO_URI, FRONT_URL } = process.env;

  return {
    env: NODE_ENV,
    http: {
      frontUrl: FRONT_URL,
      baseUrl: BASE_URL,
      port: PORT,
    },
    database: {
      mongodb: {
        uri: MONGO_URI,
      },
    },
  };
};

export default config;
