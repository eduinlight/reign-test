import * as Joi from 'joi';

const envConfigSchema = Joi.object({
  BASE_URL: Joi.string().required(),
  PORT: Joi.number().default(9000),
  MONGO_URI: Joi.string().required(),
  NODE_ENV: Joi.string()
    .valid('development', 'production', 'test', 'provision')
    .default('development'),
  FRONT_URL: Joi.string().required(),
});

export default envConfigSchema;
