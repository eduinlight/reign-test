# Reign test API
- name: Eduin Garcia Cordero
- linkedin: [https://www.linkedin.com/in/eduinlight/](https://www.linkedin.com/in/eduinlight/)
- github: [https://github.com/eduinlight](https://github.com/eduinlight)
- email: [eduinlight@gmail.com](mailto:eduinlight@gmail.com)
- stack: Nodejs + Nestjs + Mongodb + GIT + Docker + ESlint + Typescript + Gitlab-CI

## Before install

Download and install git and docker. Be shure that it works on your terminal.

## For Development

This api is containarized so you do not need to have nothing more than docker and internet connection to be able to use it. Run this command on the project api directory.
```bash
# This is going to build the needed images to run your development environment and run them in background
docker-compose up -d --build 
```

This are the containers name that needs to be running:
- reign-test-mongodb
- reign-test-api

You can check if the containers are correctly running with:
```bash
docker ps
```

In package.json I create the script **docker** use it to lunch any other script of the project.

Now you can install npm dependencies:

```bash
npm run docker npm i
```

Now you can use any of the next commands to start the development environment:

```bash
npm run dev
# or
npm run docker npm run start:dev
```

## For Production

To start the API for production just execute this simple script that starts the containers: reign-test-mongodb, reign-test-api and reign-test-mongodb-backup 

```bash
docker-compose -f docker-compose-prod.yml up --build
```

## Run tests

```bash
npm run docker npm run test
```
